﻿using System;
using System.Collections.Generic;

namespace Laundry.Domain.ValueObject
{
    public class Money
    {
        public static Money ZERO => new Money(0);

        public decimal Amount { get; }

        public Money(decimal amount)
        {
            Amount = amount;
        }

        public static Money operator -(Money money1, Money money2)
        {
            return new Money(money1.Amount - money2.Amount);
        }

        public static bool operator !=(Money money1, Money money2)
        {
            return !(money1 == money2);
        }

        public static Money operator *(Money money, decimal multiplier)
        {
            return new Money(money.Amount * multiplier);
        }

        public static Money operator /(Money money, decimal divisor)
        {
            return new Money(money.Amount / divisor);
        }

        public static Money operator +(Money money1, Money money2)
        {
            return new Money(money1.Amount + money2.Amount);
        }

        public static bool operator ==(Money money1, Money money2)
        {
            return EqualityComparer<Money>.Default.Equals(money1, money2);
        }

        public override bool Equals(object obj)
        {
            var money = obj as Money;
            return money != null &&
                   Amount == money.Amount;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Amount);
        }
    }
}