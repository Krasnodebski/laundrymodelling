﻿using Laundry.Domain.Laundry;

namespace Laundry.Domain.Payment.Policies
{
    public abstract class LaundryServicePricingPolicy
    {
        protected LaundryServicePricingPolicy Next { get; }

        public LaundryServicePricingPolicy(LaundryServicePricingPolicy next)
        {
            Next = next;
        }

        public abstract LaundryOffer ModifyPrice(LaundryOffer offer, Client.Client client);
    }
}