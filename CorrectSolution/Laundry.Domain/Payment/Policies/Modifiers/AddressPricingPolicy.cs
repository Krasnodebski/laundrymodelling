﻿using System;
using Laundry.Domain.Laundry;

namespace Laundry.Domain.Payment.Policies.Modifiers
{
    public sealed class AddressPricingPolicy : LaundryServicePricingPolicy
    {
        public AddressPricingPolicy(LaundryServicePricingPolicy next) : base(next) { }

        public override LaundryOffer ModifyPrice(LaundryOffer offer, Client.Client client)
        {
            // element łańcucha pricingu - ten na przykład doliczy dodatkowe opłaty jeśli adres klienta będzie daleko
            throw new NotImplementedException();
        }
    }
}
