﻿using Laundry.Domain.Laundry;

namespace Laundry.Domain.Payment.Strategies
{
    public interface IInitialLaundryServiceValuationStrategy
    {
        LaundryOffer Valuate(LaundryOffer offer);
    }
}
