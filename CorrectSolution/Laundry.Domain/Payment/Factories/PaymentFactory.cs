﻿using Laundry.Domain.Laundry;

namespace Laundry.Domain.Payment.Factories
{
    public static class PaymentFactory
    {
        public static Payment Create(Payer payer, LaundryOrder order)
        {
            return new Payment(payer, order);
        }
    }
}
