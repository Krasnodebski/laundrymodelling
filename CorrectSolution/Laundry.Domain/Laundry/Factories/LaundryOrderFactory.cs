﻿using Laundry.Domain.Client;
using Laundry.Domain.Client.Repositories.Interfaces;
using System;

namespace Laundry.Domain.Laundry.Factories
{
    public class LaundryOrderFactory
    {
        private readonly IAddressRepository _addressRepository;

        public LaundryOrderFactory(IAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        public LaundryOrder Create(Client.Client client, LaundryOffer offer)
        {
            Address address = _addressRepository.Find(offer.AddressId);
            if (!client.HasAssignedAddressById(address.Id))
                throw new Exception(); //todo own ex

            return new LaundryOrder(client, address, offer.CollectionDate, offer.DeliveryDate, offer.Content, offer.Price);
        }
    }
}
