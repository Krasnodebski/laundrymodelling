﻿using System.Collections.Generic;

namespace Laundry.Domain.Laundry.Strategies
{
    public interface ILaundryWeightCalculationStrategy
    {
        int CalculateWeight(Dictionary<LaundryType, int> laundry);
    }
}
