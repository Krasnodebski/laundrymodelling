﻿using Laundry.Domain.DTO;
using Laundry.Domain.ValueObject;
using System;
using System.Collections.Generic;

namespace Laundry.Domain.Laundry
{
    public sealed class LaundryOffer : ICloneable
    {
        public Guid AddressId { get; private set; }
        public DateTime CollectionDate { get; private set; }
        public Dictionary<LaundryType, int> Content { get; private set; }
        public DateTime DeliveryDate { get; private set; }
        public Money Price { get; private set; }

        public LaundryOffer(LaundryOfferCriteria criteria)
        {
            CollectionDate = criteria.CollectionDate;
            DeliveryDate = criteria.DeliveryDate;
            Content = criteria.Content;
            AddressId = criteria.AddressId;
        }

        private LaundryOffer()
        {
        }

        public static bool operator !=(LaundryOffer offer1, LaundryOffer offer2)
        {
            return !(offer1 == offer2);
        }

        public static bool operator ==(LaundryOffer offer1, LaundryOffer offer2)
        {
            return EqualityComparer<LaundryOffer>.Default.Equals(offer1, offer2);
        }

        public LaundryOffer ChangeAddress(Guid addressId)
        {
            LaundryOffer offer = (LaundryOffer)Clone();
            offer.AddressId = addressId;
            return offer;
        }

        public LaundryOffer ChangeCollectionDate(DateTime collectionDate)
        {
            LaundryOffer offer = (LaundryOffer)Clone();
            offer.CollectionDate = collectionDate;
            return offer;
        }

        public LaundryOffer ChangeContent(Dictionary<LaundryType, int> content)
        {
            LaundryOffer offer = (LaundryOffer)Clone();
            offer.Content = content;
            return offer;
        }

        public LaundryOffer ChangeDeliveryDate(DateTime deliveryDate)
        {
            LaundryOffer offer = (LaundryOffer)Clone();
            offer.DeliveryDate = deliveryDate;
            return offer;
        }
        public LaundryOffer ChangePrice(Money price)
        {
            LaundryOffer offer = (LaundryOffer)Clone();
            offer.Price = price;
            return offer;
        }

        public object Clone()
        {
            return new LaundryOffer()
            {
                AddressId = AddressId,
                CollectionDate = CollectionDate,
                Content = Content,
                DeliveryDate = DeliveryDate,
                Price = Price
            };
        }

        public override bool Equals(object obj)
        {
            var offer = obj as LaundryOffer;
            return offer != null &&
                   CollectionDate == offer.CollectionDate &&
                   DeliveryDate == offer.DeliveryDate &&
                   EqualityComparer<Dictionary<LaundryType, int>>.Default.Equals(Content, offer.Content) &&
                   EqualityComparer<Money>.Default.Equals(Price, offer.Price) &&
                   AddressId.Equals(offer.AddressId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CollectionDate, DeliveryDate, Content, Price, AddressId);
        }
    }
}