﻿using Laundry.Domain.Client;
using Laundry.Domain.ValueObject;
using System;
using System.Collections.Generic;

namespace Laundry.Domain.Laundry
{
    public class LaundryOrder
    {
        public Address Address { get; }
        public DateTime CollectionDate { get; }
        public Dictionary<LaundryType, int> Content { get; }
        public DateTime DeliveryDate { get; }
        public Guid Id { get; }
        public Client.Client Owner { get; }
        public Money Value { get; }
        public bool RequiresPaymentInAdvance => Owner.HasToPayInAdvanceFor(this);

        internal LaundryOrder(Client.Client client, Address address, DateTime collectionDate, DateTime deliveryDate, Dictionary<LaundryType, int> content, Money value)
        {
            Owner = client;
            Address = address;
            CollectionDate = collectionDate;
            DeliveryDate = deliveryDate;
            Content = content;
            Value = value;
        }

        public bool IsPropertyOf(Guid clientId) => Owner.Id == clientId;
    }

    public enum LaundryType
    {
        Carpet,
        Suit,
        Shirt
        // etc
    }
}