﻿using Laundry.Domain.Laundry;
using System;
using System.Collections.Generic;

namespace Laundry.Domain.DTO
{
    public class LaundryOfferCriteria
    {
        public Guid AddressId { get; set; }
        public DateTime CollectionDate { get; set; }
        public Dictionary<LaundryType, int> Content { get; set; }
        public DateTime DeliveryDate { get; set; }
    }
}