﻿using System;

namespace Laundry.Domain.Client.Repositories.Interfaces
{
    public interface IAddressRepository
    {
        Address Find(Guid guid);
    }
}
