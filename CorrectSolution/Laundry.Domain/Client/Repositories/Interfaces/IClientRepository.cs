﻿using System;

namespace Laundry.Domain.Client.Repositories.Interfaces
{
    public interface IClientRepository
    {
        Client Find(Guid guid);
    }
}