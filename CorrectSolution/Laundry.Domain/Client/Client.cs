﻿using Laundry.Domain.Laundry;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laundry.Domain.Client
{
    public class Client
    {
        public List<Address> Addreses { get; }
        public ClientType ClientType { get; }
        public Guid Id { get; }
        public int LoyaltyPoints { get; }
        public ICollection<LaundryOrder> Orders { get; }
        public Subscription Subscription { get; }

        public bool HasAssignedAddressById(Guid addressId)
        {
            return Addreses.Any(a => a.Id == addressId);
        }

        public bool HasToPayInAdvanceFor(LaundryOrder laundryOrder)
        {
            throw new NotImplementedException();
        }
    }

    public enum ClientType
    {
    }
}