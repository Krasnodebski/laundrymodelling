﻿using System;

namespace Laundry.Domain.Courier.Repositories
{
    public interface ICourierRepository
    {
        Courier FindCourierTransportingOrder(Guid orderId);

        int Update(Courier courier);
    }
}