﻿using Laundry.Domain.Laundry;
using Laundry.Domain.Laundry.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laundry.Domain.Courier
{
    public class Courier
    {
        // jakoś to będzie wstrzyknięte
        public ILaundryWeightCalculationStrategy LaundryWeightCalculationStrategy { get; }

        public Guid Id { get; }

        public List<LaundryOrder> Orders { get; }

        public int MaximumLoad { get; }

        public PickupResult PickupOrder(LaundryOrder order)
        {
            if (!CanTransport(order.Content, order.CollectionDate, order.DeliveryDate))
                return PickupResult.MaximumLoadExceeded;
            return PickupResult.Success;
        }

        public bool CanTransport(Dictionary<LaundryType, int> content, DateTime collectionDate, DateTime deliveryDate)
        {
            int laundryWeight = LaundryWeightCalculationStrategy.CalculateWeight(content);
            // sprawdzamy czy dopuszczalne obłożenie nie zostanie przekroczone jeśli kurier wziąłby tę paczkę
            // (biorąc pod uwagę pozostałe paczki)
            return true;
        }

        public void CancelPickupOrderById(Guid orderId, Guid clientId)
        {
            var order = Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null || !order.IsPropertyOf(clientId))
                return;

            Orders.Remove(order);
        }
    }

    public enum PickupResult
    {
        MaximumLoadExceeded,
        Success
    }
}
