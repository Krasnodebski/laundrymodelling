﻿using System;

namespace Laundry.DailySchedule
{
    public interface IDailyScheduleService
    {
        DailySchedule GetScheduleFor(Guid employeeId);

        void SubmitDailyReport(DailyReport report, Guid employeeId);
    }
}
