﻿using Laundry.Domain.Laundry;
using System;

namespace Laundry.Application.Services.Interfaces
{
    public interface ILaundryService
    {
        OrderResult Order(Guid clientId, LaundryOffer offer);

        void CancelOrder(Guid clientId, Guid orderId);
    }

    public enum OrderResult
    {
        InvalidOrExpiredOffer,
        ClientNotFound,
        AddressNotFound,
        Success,
        IncorrectAddress,
        SchedulingFailed,
        PaymentFailed
    }
}
