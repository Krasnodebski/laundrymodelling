﻿using Laundry.Domain.DTO;
using Laundry.Domain.Laundry;

namespace Laundry.Application.Services.Interfaces
{
    public interface IScheduleTransport
    {
        bool CanTransport(LaundryOfferCriteria offerCriteria);

        ScheduleResult Schedule(LaundryOrder order);
    }

    public enum ScheduleResult
    {
        NoAvailableCourierFound,
        Success
    }
}