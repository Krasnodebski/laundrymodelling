﻿using Laundry.Domain.DTO;
using Laundry.Domain.Laundry;
using System;
using System.Collections.Generic;

namespace Laundry.Application.Services.Interfaces
{
    public interface IOfferService
    {
        IEnumerable<LaundryOffer> CreateOffer(Guid clientId, LaundryOfferCriteria offerCriteria);
        OfferValidationResult ValidateOffer(Guid clientId, LaundryOffer offer);
    }

    public enum OfferValidationResult
    {
        ClientNotFound,
        AddressNotFound,
        IncorrectAddress,
        InvalidOrExpiredOffer,
        Success
    }
}
