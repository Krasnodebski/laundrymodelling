﻿using Laundry.Application.Services.Interfaces;
using Laundry.Domain.Laundry;
using Laundry.Domain.Payment;
using Laundry.Domain.Payment.Factories;
using Laundry.Domain.Payment.Repositories;

namespace Laundry.Application.Services.Implementations
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public Payment RegisterPaymentFor(LaundryOrder order)
        {
            Payer payer = _paymentRepository.FindPayerByClient(order.Owner);
            if (payer == null)
            {
                return null;
            }

            // pobrać automatycznie z karty na przykład

            Payment payment = PaymentFactory.Create(payer, order); 
            _paymentRepository.Save(payment);

            return payment;
        }
    }
}
