﻿using Laundry.Application.Services.Interfaces;
using Laundry.Domain.DTO;

namespace Laundry.Application.Services.Implementations
{
    internal class LaundryCapabilityCalculator : ICalculateLaundryCapability
    {
        public bool CanProcess(LaundryOfferCriteria offer)
        {
            // wyliczyć kiedy może być realizowane zamówienie (w jakie konkretne dni i godziny)
            // pobrać możliwości pralni (np. z bazy)
            // pobrać zamówienia które będą realizowane też w tych dniach/godzinach
            // sprawdzić czy można jeszcze zrealizować to zamówienie
            return true;
        }
    }
}