﻿using Laundry.Application.Services.Interfaces;
using Laundry.Domain.Client;
using Laundry.Domain.Client.Repositories.Interfaces;
using Laundry.Domain.DTO;
using Laundry.Domain.Laundry;
using Laundry.Domain.Payment.Factories;
using Laundry.Domain.Payment.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laundry.Application.Services.Implementations
{
    public class OfferService : IOfferService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly ICalculateLaundryCapability _laundryCapabilityCalculator;
        private readonly IScheduleTransport _transportScheduler;

        public OfferService(IClientRepository clientRepository, IAddressRepository addressRepository,
            ICalculateLaundryCapability laundryCapabilityCalculator, IScheduleTransport transportScheduler)
        {
            _clientRepository = clientRepository;
            _addressRepository = addressRepository;
            _laundryCapabilityCalculator = laundryCapabilityCalculator;
            _transportScheduler = transportScheduler;
        }

        public IEnumerable<LaundryOffer> CreateOffer(Guid clientId, LaundryOfferCriteria offerCriteria)
        {
            Client client = _clientRepository.Find(clientId);
            if (client == null)
                throw new Exception(); //todo own ex

            bool canBeProcessed = _laundryCapabilityCalculator.CanProcess(offerCriteria);
            bool canBeTransported = _transportScheduler.CanTransport(offerCriteria);
            if (!(canBeProcessed && canBeTransported))
                return CreateAnotherOffers(client, offerCriteria);

            if (!client.HasAssignedAddressById(offerCriteria.AddressId))
                throw new Exception(); //todo own ex

            var offer = new LaundryOffer(offerCriteria);

            IInitialLaundryServiceValuationStrategy initialPriceStrategy = InitialLaundryServiceValuationStrategyFactory.Create();
            offer = initialPriceStrategy.Valuate(offer);

            var laundryOfferMods = LaundryServicePricingPolicyFactory.Create();
            offer = laundryOfferMods.ModifyPrice(offer, client);

            return new List<LaundryOffer> { offer };
        }

        private IEnumerable<LaundryOffer> CreateAnotherOffers(Client client, LaundryOfferCriteria offerCriteria)
        {
            // zaproponowanie alternatywnej oferty/ofert
            return new List<LaundryOffer>();
        }

        public OfferValidationResult ValidateOffer(Guid clientId, LaundryOffer offer)
        {
            Client client = _clientRepository.Find(clientId);
            if (client == null)
                return OfferValidationResult.ClientNotFound;

            Address address = _addressRepository.Find(offer.AddressId);
            if (address == null)
                return OfferValidationResult.AddressNotFound;

            if (client.HasAssignedAddressById(address.Id))
                return OfferValidationResult.IncorrectAddress;

            var offerCriteria = new LaundryOfferCriteria()
            {
                AddressId = offer.AddressId,
                Content = offer.Content,
                CollectionDate = offer.CollectionDate,
                DeliveryDate = offer.DeliveryDate
            };

            var recreatedOffer = CreateOffer(clientId, offerCriteria);
            var anySameOffer = recreatedOffer.Any(o => o == offer);
            if (!anySameOffer)
                return OfferValidationResult.InvalidOrExpiredOffer;

            return OfferValidationResult.Success;
        }
    }
}
