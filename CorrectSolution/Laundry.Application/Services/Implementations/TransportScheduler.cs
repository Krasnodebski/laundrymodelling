﻿using Laundry.Application.Services.Interfaces;
using Laundry.Domain.Courier;
using Laundry.Domain.DTO;
using Laundry.Domain.Laundry;
using System.Collections.Generic;

namespace Laundry.Application.Services.Implementations
{
    internal class TransportScheduler : IScheduleTransport
    {
        public bool CanTransport(LaundryOfferCriteria offerCriteria)
        {
            List<Courier> couriers = new List<Courier>(); //wyciągamy z repo odpowiednich kurierów - może jakoś posortowanych względem obłożenia, odległości
            foreach (var courier in couriers)
            {
                if (courier.CanTransport(offerCriteria.Content, offerCriteria.CollectionDate, offerCriteria.DeliveryDate))
                    return true;
            }
            return false;
        }

        public ScheduleResult Schedule(LaundryOrder order)
        {
            List<Courier> couriers = new List<Courier>(); //wyciągamy z repo odpowiednich kurierów - może jakoś posortowanych względem obłożenia, odległości
            foreach (var courier in couriers)
            {
                PickupResult result = courier.PickupOrder(order);
                if (result == PickupResult.Success)
                    return ScheduleResult.Success;
            }
            return ScheduleResult.NoAvailableCourierFound;
        }
    }
}
