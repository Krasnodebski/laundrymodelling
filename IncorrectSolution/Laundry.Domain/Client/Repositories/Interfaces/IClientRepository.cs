﻿using Laundry.Domain.Laundry;
using System;
using System.Collections.Generic;

namespace Laundry.Domain.Client.Repositories.Interfaces
{
    public interface IClientRepository
    {
        IEnumerable<LaundryOrder> GetAllClientOrdersFromThisWeek(Guid clientId);

        IEnumerable<Client> GetClientsByType(ClientType type);

        Client Find(Guid guid);
    }
}