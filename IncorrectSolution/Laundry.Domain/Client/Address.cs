﻿using System;
using System.Collections.Generic;

namespace Laundry.Domain.Client
{
    public class Address
    {
        // dane adresowe
        public Guid Id { get; }
        public Client Client { get; }
    }
}
