﻿using Laundry.Domain.Laundry;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Laundry.Domain.Client
{
    public class Client
    {
        public List<Address> Addreses { get; set; }
        public ClientType ClientType { get; set; }
        public Guid Id { get; set; }
        public int LoyaltyPoints { get; set; }
        public ICollection<LaundryOrder> Orders { get; set; }
        public Subscription Subscription { get; set; }

        public bool HasAssignedAddressById(Guid addressId)
        {
            return Addreses.Any(a => a.Id == addressId);
        }

        public bool HasToPayInAdvanceFor(LaundryOrder laundryOrder)
        {
            throw new NotImplementedException();
        }

        public void RemoveOrderById(Guid orderId)
        {
            var order = Orders.FirstOrDefault(o => o.Id == orderId);
            Orders.Remove(order);
        }

        public void AddOrder(LaundryOrder laundryOrder)
        {
            Orders.Add(laundryOrder);
        }
    }

    public enum ClientType
    {
    }
}