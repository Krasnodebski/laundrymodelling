﻿using System;
using System.Collections.Generic;

namespace Laundry.Domain.Courier
{
    public class Courier
    {
        public Guid Id { get; set; }

        public List<Client.Client> Clients { get; set; }

        public int MaximumLoad { get; set; }

        public bool CanTransport(int weight, DateTime collectionDate, DateTime deliveryDate)
        {
            return true;
        }
    }

    public enum PickupResult
    {
        MaximumLoadExceeded,
        Success
    }
}
