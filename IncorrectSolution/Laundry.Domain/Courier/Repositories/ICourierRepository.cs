﻿using System;
using System.Collections.Generic;

namespace Laundry.Domain.Courier.Repositories
{
    public interface ICourierRepository
    {
        IEnumerable<Courier> GetAll();

        Courier FindCourierTransportingOrder(Guid orderId);

        int Update(Courier courier);
    }
}