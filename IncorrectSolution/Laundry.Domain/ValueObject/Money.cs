﻿using System;

namespace Laundry.Domain.ValueObject
{
    public class Money
    {
        public static Money ZERO => new Money(Guid.NewGuid(), 0);

        public Guid Id { get; set; }

        public decimal Amount { get; set; }

        public Money(Guid id, decimal amount)
        {
            Id = id;
            Amount = amount;
        }
    }
}