﻿namespace Laundry.Domain.Payment.Repositories
{
    public interface IPaymentRepository
    {
        Payer FindPayerByClient(Client.Client client);

        void Save(Payment payment);
    }
}