﻿using Laundry.Domain.Laundry;

namespace Laundry.Domain.Payment
{
    public class Payment
    {
        public LaundryOrder Order { get; }

        public Payer Payer { get; }

        public Payment(Payer payer, LaundryOrder order)
        {
            Payer = payer;
            Order = order;
        }
    }
}