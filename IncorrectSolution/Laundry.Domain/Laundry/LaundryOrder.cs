﻿using Laundry.Domain.Client;
using Laundry.Domain.ValueObject;
using System;
using System.Collections.Generic;

namespace Laundry.Domain.Laundry
{
    public class LaundryOrder
    {
        public Address Address { get; set; }
        public DateTime CollectionDate { get; set; }
        public Dictionary<LaundryType, int> Content { get; set; }
        public DateTime DeliveryDate { get; set; }
        public Guid Id { get; set; }
        public Client.Client Owner { get; set; }
        public Courier.Courier Courier { get; set; }
        public Money Value { get; set; }
        public bool RequiresPaymentInAdvance => Owner.HasToPayInAdvanceFor(this);

        public LaundryOrder(Address address, DateTime collectionDate, DateTime deliveryDate, Dictionary<LaundryType, int> content, Money value)
        {
            Address = address;
            CollectionDate = collectionDate;
            DeliveryDate = deliveryDate;
            Content = content;
            Value = value;
        }

        public bool IsPropertyOf(Guid clientId) => Owner.Id == clientId;
    }

    public enum LaundryType
    {
        Carpet,
        Suit,
        Shirt
        // etc
    }
}