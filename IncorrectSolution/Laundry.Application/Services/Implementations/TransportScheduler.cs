﻿using Laundry.Application.Services.Interfaces;
using Laundry.Application.Utils;
using Laundry.Domain.Courier;
using Laundry.Domain.DTO;
using Laundry.Domain.Laundry;
using System.Collections.Generic;

namespace Laundry.Application.Services.Implementations
{
    internal class TransportScheduler : IScheduleTransport
    {
        public bool CanTransport(LaundryOfferCriteria offerCriteria)
        {
            var laundryWeight = LaundryWeightCalculationUtil.CalculateWeight(offerCriteria.Content);
            List<Courier> couriers = new List<Courier>(); //wyciągamy z repo odpowiednich kurierów - może jakoś posortowanych względem obłożenia, odległości
            foreach (var courier in couriers)
            {
                if (courier.CanTransport(laundryWeight, offerCriteria.CollectionDate, offerCriteria.DeliveryDate))
                    return true;
            }
            return false;
        }

        public ScheduleResult Schedule(LaundryOrder order)
        {
            var laundryWeight = LaundryWeightCalculationUtil.CalculateWeight(order.Content);
            List<Courier> couriers = new List<Courier>(); //wyciągamy z repo odpowiednich kurierów - może jakoś posortowanych względem obłożenia, odległości
            foreach (var courier in couriers)
            {
                bool canTransport = courier.CanTransport(laundryWeight, order.CollectionDate, order.DeliveryDate);
                if (canTransport)
                {
                    order.Courier = courier;
                    return ScheduleResult.Success;
                }
            }
            return ScheduleResult.NoAvailableCourierFound;
        }
    }
}
