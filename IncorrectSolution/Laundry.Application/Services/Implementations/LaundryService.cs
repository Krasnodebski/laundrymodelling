﻿using Laundry.Application.Services.Interfaces;
using Laundry.Domain.Client;
using Laundry.Domain.Client.Repositories.Interfaces;
using Laundry.Domain.Courier;
using Laundry.Domain.Courier.Repositories;
using Laundry.Domain.Laundry;
using Laundry.Domain.Payment;
using System;
using System.Linq;

namespace Laundry.Application.Services.Implementations
{
    class LaundryService : ILaundryService
    {
        private readonly IOfferService _offerService;
        private readonly IPaymentService _paymentService;
        private readonly IClientRepository _clientRepository;
        private readonly ICourierRepository _courierRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IScheduleTransport _transportScheduler;

        public LaundryService(IOfferService offerService, IPaymentService paymentService, 
            IClientRepository clientRepository, ICourierRepository courierRepository, 
            IScheduleTransport transportScheduler)
        {
            _offerService = offerService;
            _paymentService = paymentService;
            _clientRepository = clientRepository;
            _courierRepository = courierRepository;
            _transportScheduler = transportScheduler;
        }

        public void CancelOrder(Guid clientId, Guid orderId)
        {
            Courier courier = _courierRepository.FindCourierTransportingOrder(orderId);
            courier.Clients.FirstOrDefault(c => c.Id == clientId).RemoveOrderById(orderId);
            _courierRepository.Update(courier);
        }

        public OrderResult Order(Guid clientId, LaundryOffer offer)
        {
            OfferValidationResult validationResult = _offerService.ValidateOffer(clientId, offer);
            if (validationResult == OfferValidationResult.InvalidOrExpiredOffer)
                return OrderResult.InvalidOrExpiredOffer;

            if (validationResult == OfferValidationResult.ClientNotFound)
                return OrderResult.ClientNotFound;

            if (validationResult == OfferValidationResult.AddressNotFound)
                return OrderResult.AddressNotFound;

            if (validationResult == OfferValidationResult.IncorrectAddress)
                return OrderResult.IncorrectAddress;

            Address address = _addressRepository.Find(offer.AddressId);
            LaundryOrder order = new LaundryOrder(address, offer.CollectionDate, offer.DeliveryDate, offer.Content, offer.Price);
            var schedulingResult = _transportScheduler.Schedule(order);
            if (schedulingResult != ScheduleResult.Success)
                return OrderResult.SchedulingFailed;

            Client client = _clientRepository.Find(clientId);
            order.Owner = client;

            if (order.RequiresPaymentInAdvance)
            {
                Payment payment = _paymentService.RegisterPaymentFor(order);
                if (payment == null)
                {
                    return OrderResult.PaymentFailed;
                }
            }

            return OrderResult.Success;
        }
    }
}
