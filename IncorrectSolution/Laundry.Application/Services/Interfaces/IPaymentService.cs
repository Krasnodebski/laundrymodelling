﻿using Laundry.Domain.Laundry;
using Laundry.Domain.Payment;

namespace Laundry.Application.Services.Interfaces
{
    public interface IPaymentService
    {
        Payment RegisterPaymentFor(LaundryOrder order);
    }
}
