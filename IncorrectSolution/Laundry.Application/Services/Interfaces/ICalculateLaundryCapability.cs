﻿using Laundry.Domain.DTO;

namespace Laundry.Application.Services.Interfaces
{
    public interface ICalculateLaundryCapability
    {
        bool CanProcess(LaundryOfferCriteria offer);
    }
}
