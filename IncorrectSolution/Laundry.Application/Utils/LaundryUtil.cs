﻿using Laundry.Domain.Client;
using Laundry.Domain.Laundry;
using Laundry.Domain.ValueObject;
using System;

namespace Laundry.Application.Utils
{
    public static class LaundryUtil
    {
        public static Money CalculateInitialOfferPrice(LaundryOffer offer)
        {
            return new Money(Guid.NewGuid(), 0);
        }

        internal static Money CalculatePriceAfterDiscounts(LaundryOffer offer, Client client)
        {
            return new Money(Guid.NewGuid(), 0);
        }
    }
}
